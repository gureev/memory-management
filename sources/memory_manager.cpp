#include "ph/ph.hpp"

#include "sources/memory_manager.hpp"

//------------------------------------------------------------------------------

MemoryManager::MemoryManager( MemoryPtr _memoryPtr, int _memorySize, int _blockSize )
	:	m_memoryPtr{ _memoryPtr }
	,	m_memorySize{ _memorySize }
	,	m_blockSize{ _blockSize }
{
	assert( m_memorySize > 0 );
	assert( m_blockSize > 0 );
	assert( m_memorySize >= getActualBlockSize() );
	assert( !( m_memorySize % getActualBlockSize() ) );

	const int blocksCount{ getBlocksCount() };
	m_freeBlocks.reserve( static_cast< std::size_t >( blocksCount ) );

	for ( int i{ blocksCount - 1 }; i >= 0; --i )
		m_freeBlocks.emplace_back( getBlock( i ) );
}

//------------------------------------------------------------------------------

MemoryManager::~MemoryManager() = default;

//------------------------------------------------------------------------------

void *
MemoryManager::allocate()
{
	if ( m_freeBlocks.empty() )
		return nullptr;

	MemoryPtr nextBlock{ m_freeBlocks.back() };
	m_freeBlocks.pop_back();

	return static_cast< void * >( nextBlock );
}

//------------------------------------------------------------------------------

void
MemoryManager::free( void * _block )
{
	if ( !_block )
		return;

	auto concreteBlock{ static_cast< MemoryPtr >( _block ) };

	assert( hasBlock( concreteBlock ) );
	assert( isBlockBusy( concreteBlock ) );

	m_freeBlocks.emplace_back( concreteBlock );
}

//------------------------------------------------------------------------------

int
MemoryManager::getBlocksCount() const
{
	return m_memorySize / getActualBlockSize();
}

//------------------------------------------------------------------------------

int
MemoryManager::getFreeBlocksCount() const
{
	return static_cast< int >( m_freeBlocks.size() );
}

//------------------------------------------------------------------------------

int
MemoryManager::getBusyBlocksCount() const
{
	return getBlocksCount() - getFreeBlocksCount();
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockBusy( void * _block ) const
{
	return isBlockBusy( static_cast< MemoryPtr >( _block ) );
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockFree( void * _block ) const
{
	return isBlockFree( static_cast< MemoryPtr >( _block ) );
}

//------------------------------------------------------------------------------

MemoryManager::MemoryPtr
MemoryManager::getBlock( int _index ) const
{
	assert( _index >= 0 );
	assert( _index < getBlocksCount() );
	return m_memoryPtr + _index * getActualBlockSize();
}

//------------------------------------------------------------------------------

int
MemoryManager::getActualBlockSize() const
{
	return m_blockSize;
}

//------------------------------------------------------------------------------

bool
MemoryManager::hasBlock( MemoryPtr _block ) const
{
	return	_block >= m_memoryPtr
		&&	_block < ( m_memoryPtr + m_memorySize )
	;
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockBusy( MemoryPtr _block ) const
{
	assert( hasBlock( _block ) );

	// NOTE: Now used only for tests, no need to optimize algorithm

	for ( auto freeBlock : m_freeBlocks )
		if ( freeBlock == _block )
			return false;

	return true;
}

//------------------------------------------------------------------------------

bool
MemoryManager::isBlockFree( MemoryPtr _block ) const
{
	assert( hasBlock( _block ) );
	return !isBlockBusy( _block );
}

//------------------------------------------------------------------------------
