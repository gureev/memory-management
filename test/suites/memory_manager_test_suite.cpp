#include "ph/ph.hpp"

#include "sources/memory_manager.hpp"

#include "test/engine/test_case.hpp"

//------------------------------------------------------------------------------

namespace Tests {

//------------------------------------------------------------------------------

/*

	Test Plan:

	Done	1) Single block
	Done	2) Single block re-allocated
	Done	3) Two blocks
	Done	4) Two blocks with only first re-allocated
	Done	5) Two blocks with only second re-allocated
	Done	6) Two blocks with both re-allocated
	Done	7) Allocate too much blocks
	IMPOSS	8) Free same block twice
	Done	9) Re-allocate same block several times
	Done	10) Re-allocate block between busy blocks
	Done	11) Re-allocate block between free blocks
	Done	12) Re-use same block after several allocate/free cycles

*/

//------------------------------------------------------------------------------

auto MemoryZero{ static_cast< MemoryManager::MemoryType >( 0 ) };

//------------------------------------------------------------------------------

constexpr auto
getMemorySize( int /*_blocksCount*/ )
{
	return 100;
}

//------------------------------------------------------------------------------

constexpr auto
getBlockSize( int _memorySize, int _blocksCount )
{
	return _memorySize / _blocksCount;
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_1_SingleBlock )
{
	constexpr int blocksCount{ 1 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 1 );

	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock{ mm.allocate() };

	assert( mm.isBlockBusy( testBlock ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 1 );

	mm.free( testBlock );

	assert( mm.isBlockFree( testBlock ) );
	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_2_SingleBlockReAllocated )
{
	constexpr int blocksCount{ 1 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 1 );

	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock{ mm.allocate() };
	mm.free( testBlock );
	testBlock = mm.allocate();

	assert( mm.isBlockBusy( testBlock ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 1 );

	mm.free( testBlock );

	assert( mm.isBlockFree( testBlock ) );
	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_3_TwoBlocks )
{
	constexpr int blocksCount{ 2 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 2 );

	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock1{ mm.allocate() };

	assert( mm.isBlockBusy( testBlock1 ) );
	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 1 );

	void * testBlock2{ mm.allocate() };

	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 2 );

	mm.free( testBlock1 );

	assert( mm.isBlockFree( testBlock1 ) );
	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 1 );

	mm.free( testBlock2 );

	assert( mm.isBlockFree( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_4_TwoBlocksWithOnlyFirstReAllocated )
{
	constexpr int blocksCount{ 2 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 2 );

	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };

	mm.free( testBlock1 );
	testBlock1 = mm.allocate();

	assert( mm.isBlockBusy( testBlock1 ) );
	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 2 );

	mm.free( testBlock1 );
	mm.free( testBlock2 );

	assert( mm.isBlockFree( testBlock1 ) );
	assert( mm.isBlockFree( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_5_TwoBlocksWithOnlySecondReAllocated )
{
	constexpr int blocksCount{ 2 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 2 );

	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };

	mm.free( testBlock2 );
	testBlock2 = mm.allocate();

	assert( mm.isBlockBusy( testBlock1 ) );
	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 2 );

	mm.free( testBlock1 );
	mm.free( testBlock2 );

	assert( mm.isBlockFree( testBlock1 ) );
	assert( mm.isBlockFree( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_6_TwoBlocksWithBothReAllocated )
{
	constexpr int blocksCount{ 2 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 2 );

	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };

	mm.free( testBlock1 );
	mm.free( testBlock2 );

	testBlock1 = mm.allocate();
	testBlock2 = mm.allocate();

	assert( mm.isBlockBusy( testBlock1 ) );
	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 2 );

	mm.free( testBlock1 );
	mm.free( testBlock2 );

	assert( mm.isBlockFree( testBlock1 ) );
	assert( mm.isBlockFree( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_7_AllocateTwoMuchBlocks )
{
	constexpr int blocksCount{ 4 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 4 );

	std::array< void *, 5 > testBlocks;
	for ( std::size_t i{ 0 }; i < testBlocks.size(); ++i )
		testBlocks[ i ] = mm.allocate();

	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 4 );

	assert( testBlocks[ 0 ] );
	assert( testBlocks[ 1 ] );
	assert( testBlocks[ 2 ] );
	assert( testBlocks[ 3 ] );
	assert( !testBlocks[ 4 ] );

	for ( std::size_t i{ 0 }; i < testBlocks.size(); ++i )
		mm.free( testBlocks[ i ] );

	assert( mm.getFreeBlocksCount() == 4 );
	assert( mm.getBusyBlocksCount() == 0 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_8_FreeSameBlockTwice )
{
	constexpr int blocksCount{ 1 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 1 );

	void * testBlock{ mm.allocate() };

	assert( mm.isBlockBusy( testBlock ) );

	mm.free( testBlock );

	assert( mm.isBlockFree( testBlock ) );

	// mm.free( testBlock ); // Assertion failed
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_9_ReAllocateSameBlockSeveralTimes )
{
	constexpr int blocksCount{ 10 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 10 );

	void * testBlock{ mm.allocate() };

	for ( int i{ 0 }; i < 10; ++i )
	{
		mm.free( testBlock );
		testBlock = mm.allocate();
	}

	assert( mm.isBlockBusy( testBlock ) );
	assert( mm.getFreeBlocksCount() == 9 );
	assert( mm.getBusyBlocksCount() == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_10_ReAllocateBlockBetweenBusyBlocks )
{
	constexpr int blocksCount{ 4 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 4 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };
	void * testBlock3{ mm.allocate() };

	for ( int i{ 0 }; i < 10; ++i )
	{
		mm.free( testBlock2 );
		testBlock2 = mm.allocate();
	}

	assert( mm.isBlockBusy( testBlock1 ) );
	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.isBlockBusy( testBlock3 ) );
	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 3 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_11_ReAllocateBlockBetweenFreeBlocks )
{
	constexpr int blocksCount{ 4 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 4 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };
	void * testBlock3{ mm.allocate() };

	mm.free( testBlock1 );
	mm.free( testBlock3 );

	for ( int i{ 0 }; i < 10; ++i )
	{
		mm.free( testBlock2 );
		testBlock2 = mm.allocate();
	}

	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.getFreeBlocksCount() == 3 );
	assert( mm.getBusyBlocksCount() == 1 );
}

//------------------------------------------------------------------------------

DECLARE_TEST_CASE( MemoryManager_12_ReuseSameBlockAfterSeveralAllocateFreeCycles )
{
	constexpr int blocksCount{ 5 };
	constexpr int memorySize{ getMemorySize( blocksCount ) };
	constexpr int blockSize{ getBlockSize( memorySize, blocksCount ) };
	MemoryManager::MemoryType memory[ memorySize ] = { MemoryZero };

	MemoryManager mm{ memory, memorySize, blockSize };
	assert( mm.getBlocksCount() == 5 );

	void * testBlock1{ mm.allocate() };
	void * testBlock2{ mm.allocate() };
	void * testBlock3{ mm.allocate() };
	void * testBlock4{ mm.allocate() };
	void * testBlock5{ nullptr };

	assert( mm.getFreeBlocksCount() == 1 );
	assert( mm.getBusyBlocksCount() == 4 );

	mm.free( testBlock1 );
	mm.free( testBlock2 );

	assert( mm.getFreeBlocksCount() == 3 );
	assert( mm.getBusyBlocksCount() == 2 );

	testBlock5 = mm.allocate();
	testBlock1 = mm.allocate();
	testBlock2 = mm.allocate();

	assert( mm.getFreeBlocksCount() == 0 );
	assert( mm.getBusyBlocksCount() == 5 );

	mm.free( testBlock5 );
	mm.free( testBlock1 );

	assert( mm.getFreeBlocksCount() == 2 );
	assert( mm.getBusyBlocksCount() == 3 );

	assert( mm.isBlockBusy( testBlock2 ) );
	assert( mm.isBlockBusy( testBlock3 ) );
	assert( mm.isBlockBusy( testBlock4 ) );
}

//------------------------------------------------------------------------------

} // namespace Tests

//------------------------------------------------------------------------------

